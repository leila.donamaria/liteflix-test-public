import css from "styled-jsx/css";
import {pxToRem} from "../utils/pxToRem";
export default css.global`
	:root {
	--white: #ffffff;
	--red: #ff0000;
  --black: #000000;
	--bright-blue: #0076ff;
	--white-20: rgba(255, 255, 255, 0.2);
	--black-80: rgba(0, 0, 0, 0.8);
	--vibrant-blue: #003bff;
  --black-50: rgba(0, 0, 0, 0.5);
  --dark-gray: #222222;
  --gray: #9b9b9b;
  --light-gray: #f3f3f3;
  --green: #7ed321;

	--monserrat-regular: 'Montserrat Regular';
	--monserrat-medium: 'Montserrat Medium';
	--monserrat-bold: 'Montserrat Bold';
	--robotoslab-regular: 'Roboto Slab';
	}

    /* Box sizing rules */
    *,
    *::before,
    *::after {
      box-sizing: border-box;
    }

    /* Remove default padding */
    ul[class],
    ol[class] {
      padding: 0;
    }

    /* Remove default margin */
    body,
    h1,
    h2,
    h3,
    h4,
    p,
    ul[class],
    ol[class],
    li,
    figure,
    figcaption,
    blockquote,
    dl,
    dd {
      margin: 0;
    }

    /* Set core body defaults */
    body {
      min-height: 100vh;
      scroll-behavior: smooth;
      text-rendering: optimizeSpeed;
      color: var(--white);
      font-family: var(--monserrat-regular);
      background: var(--black);
      overflow-x: hidden;
    }

    /* Remove list styles on ul, ol elements with a class attribute */
    ul[class],
    ol[class] {
      list-style: none;
    }

    /* A elements that don't have a class get default styles */
    a:not([class]) {
      text-decoration-skip-ink: auto;
    }

    /* Make images easier to work with */
    img {
      max-width: 100%;
      display: block;
    }

    /* Natural flow and rhythm in articles by default */
    article > * + * {
      margin-top: 1em;
    }

    /* React Select overwrite */

    .react-select__control {
					width: 100% !important;
					max-width: 315px !important;
					border-radius: 0 !important;
					border: 0 !important;
					border-bottom: 2px solid var(--black) !important;
					letter-spacing: 0 !important;
					font-size: ${pxToRem(16)} !important;
					font-family: var(--monserrat-regular) !important;
					font-weight: normal !important;
					color: var(--black) !important;
					text-transform: initial !important;
					padding: 0 !important;
					&:hover {
						border-bottom: 2px solid var(--bright-blue) !important;
					}
				}
				.react-select__indicators {
					display: none !important;
				}
				.react-select__menu {
					position: absolute!important;
					z-index: 4 !important;
					top: 0 !important;
					left: 0 !important;
					bottom: auto !important;
					margin: auto !important;
					max-width: 100% !important;
					width: 100% !important;
					max-width: 315px !important;
					min-height: 260px !important;
					height: auto !important;
					padding: 15px 30px 15px 0px !important;
					background: var(--white) !important;
					box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.1) !important;
				}

				.react-select__control > div {
					padding: 0 0 5px 0 !important;
				}

				.react-select__menu-list {
					width: 284px !important;
					height: 215px !important;
					background: var(--white) !important;
					border-radius: 7px !important;
					padding: 0px 10px 0px 10px !important;
				}
				.react-select__option {
					width: 235px !important;
					box-sizing: border-box !important;
					height: 40px !important;
					margin: auto !important;
					display: flex !important;
					flex-direction: row !important;
					justify-content: flex-start !important;
					align-items: center !important;
					text-transform: capitalize !important;
					letter-spacing: 0 !important;
					font-size: ${pxToRem(16)} !important;
					font-family: var(--monserrat-regular) !important;
					font-weight: normal !important;
					padding: 0 !important;
				}
				.react-select__option--is-selected {
					background: var(--white) !important;
						color: var(--black) !important
				}

        .react-select__menu-list::-webkit-scrollbar {
width: 11px;
position: fixed;
left: 100px !important;
}
.react-select__menu-list::-webkit-scrollbar-track {
background: var(--light-gray);
border-radius: 10px;
}
.react-select__menu-list::-webkit-scrollbar-thumb {
background: var(--gray);
border-radius: 10px;
}
.react-select__menu-list::-webkit-scrollbar-thumb:hover {
background: var(--gray);
}
.react-select__menu-list::-webkit-scrollbar-thumb:active {
background: var(--gray);
} 
    button {
      cursor: pointer;
    }

    a {
      display: inline-block;
      transform: translateY(0);
      &:hover {
        transform: translateY(-5px);
        transition: transform 0.3s ease-out;
      }
    }


    .hover {
						display: inline-block;
						transform: translateY(0);
						&:hover {
							transform: translateY(-5px);
							transition: transform 0.3s ease-out;
						}
					}

    /* Inherit fonts for inputs and buttons */
    input,
    button,
    textarea,
    select {
      font: inherit;
    }

    *:focus {
      outline: none;
    }

    ul {
      list-style: none;
    }

    a {
      text-decoration: none;
      color: var(--white);
    }

    /* Remove all animations and transitions for people that prefer not to see them */
    @media (prefers-reduced-motion: reduce) {
      * {
        animation-duration: 0.01ms !important;
        animation-iteration-count: 1 !important;
        transition-duration: 0.01ms !important;
        scroll-behavior: auto !important;
      }
    }

    /* 
    * Fonts 
	*/
	@font-face {
    font-family: 'Roboto Slab';
    src: url('/fonts/RobotoSlab-Regular.woff2') format('woff2'),
        url('/fonts/RobotoSlab-Regular.woff') format('woff');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
}

   @font-face {
    font-family: 'Montserrat Medium';
    src: url('/fonts/Montserrat-Medium.woff2') format('woff2'),
        url('/fonts/Montserrat-Medium.woff') format('woff');
    font-weight: 500;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: 'Montserrat Bold';
    src: url('/fonts/Montserrat-Bold.woff2') format('woff2'),
        url('/fonts/Montserrat-Bold.woff') format('woff');
    font-weight: bold;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: 'Montserrat Regular';
    src: url('/fonts/Montserrat-Regular.woff2') format('woff2'),
        url('/fonts/Montserrat-Regular.woff') format('woff');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
}


`;
