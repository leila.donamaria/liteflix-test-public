import React, {useEffect, useState, useReducer} from "react";
import Head from "next/head";
import Layout from "../components/Layout";
import Image from "../components/Image";
import Info from "../components/Info";
import Title from "../components/Title";
import {media} from "../utils/media";
import {pxToRem} from "../utils/pxToRem";
import Modal from "../components/Modal";
import {ModalContext} from "../utils/context";
import {initialState, reducer} from "../utils/reducers";
import {HorizontalGrid, VerticalGrid} from "../components/Grids";
export default function Home({pelicula_destacada, peliculas_proximamente, peliculas_populares}) {
	const [modal, dispatch] = useReducer(reducer, initialState);
	const [peliculaDestacada, setPeliculaDestacada] = useState(null);
	const [myFilms, setMyFilms] = useState([]);

	useEffect(
		() => {
			const now_playing = pelicula_destacada.sort(
				(a, b) => new Date(b.release_date) - new Date(a.release_date)
			)[0];
			setPeliculaDestacada(now_playing);
		},
		[pelicula_destacada]
	);

	useEffect(() => {
		refreshMyFilms();
	}, []);

	const refreshMyFilms = () => {
		let films = localStorage.getItem("liteflix");
		films = films ? JSON.parse(films) : [];
		setMyFilms(films);
	};

	return (
		<ModalContext.Provider value={{modalState: modal, modalDispatch: dispatch}}>
			<Layout>
				<Head>
					<title>Liteflix</title>
				</Head>
				<section>
					{modal.status && <Modal refreshMyFilms={refreshMyFilms}/>}
					<Image
						folder="external"
						name={peliculaDestacada ? peliculaDestacada.backdrop_path : ""}
						style="cover"
						alt={peliculaDestacada ? peliculaDestacada.title : ""}
					/>
					<article>
						<Info
							title={peliculaDestacada ? peliculaDestacada.title : ""}
							overview={peliculaDestacada ? peliculaDestacada.overview : ""}
						/>
						<div className="grids-appear">
							{myFilms.length > 0 && (
								<>
									<Title text="mis películas" />
									<HorizontalGrid films={myFilms.length > 4 ? myFilms.slice(0, 4) : myFilms} prop="url" />
								</>
							)}
							<Title text="próximante" />
							<HorizontalGrid films={peliculas_proximamente.slice(0, 4)} prop="backdrop_path" />
							<Title text="populares en liteflix" style="populares" />
							<VerticalGrid films={peliculas_populares.slice(0, 4)} />
						</div>
					</article>
				</section>
				<style jsx>{`
					.grids-appear {
						opacity: 0;
						animation: fadeIn 0.4s 0.3s ease-in forwards;
					}
					section {
						position: relative;
						article {
							width: 100%;
							max-width: 1100px;
							height: auto;
							margin: 0 auto;
						}
					}
					.container-user {
						width: 25px;
						height: 25px;
						margin-right: 8px;

						svg {
							width: 100%;
							height: 100%;
							display: inline-block;
						}

						&.active {
							svg {
								circle {
									fill: #ce00ff;
								}
								path {
									fill: #f5a623;
								}
							}
						}
						&:not(.active) {
							svg {
								circle {
									fill: #4a90e2;
									fill: var(--vibrant-blue);
								}
								path {
									fill: #4a90e2;
								}
							}
						}
					}

					@keyframes fadeIn{
						0% {
							opacity: 0;
							transform: translateY(50px);
						}
						100% {
							opacity: 1;
							transform: translateY(0px);
						}
					}
				`}</style>
				<style jsx global>{`
					.log-out {
						font-family: var(--monserrat-bold);
					}
					.user {
						width: 193px;
						box-sizing: border-box;
						display: flex;
						border: 0;
						flex-direction: row;
						justify-content: flex-start;
						align-items: center;
						border-radius: 20px;
						min-height: 37px;
						background: transparent;

						span {
							font-size: ${pxToRem(12)};
							color: var(--white);
							text-transform: capitalize;
						}

						@media ${media.largeDevice} {
							max-width: 100%;
						}

						&.user-active {
							background: var(--dark-gray);
							margin-bottom: 13px;
							@media ${media.largeDevice} {
								box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
								background: var(--white);
								margin-bottom: 6px;

								span {
									max-width: 67px;
									color: var(--black);
									white-space: nowrap;
									text-overflow: ellipsis;
									overflow: hidden;
								}
							}
						}
					}

					.container-user {
						width: 25px;
						height: 25px;
						margin-right: 8px;

						svg {
							width: 100%;
							height: 100%;
							display: inline-block;
						}

						&.disabled {
							& + span {
								color: #9b9b9b;
							}

							svg {
								circle {
									fill: #9b9b9b;
								}
								path {
									fill: #4a4a4a;
								}
							}
						}

						&.active {
							svg {
								circle {
									fill: #ce00ff;
								}
								path {
									fill: #f5a623;
								}
							}
						}
						&:not(.active):not(.disabled) {
							svg {
								circle {
									fill: #4a90e2;
									fill: var(--vibrant-blue);
								}
								path {
									fill: #4a90e2;
								}
							}
						}
					}
				`}</style>
			</Layout>
		</ModalContext.Provider>
	);
}
export async function getStaticProps() {
	const res1 = await fetch("https://api.themoviedb.org/3/movie/now_playing?api_key=6f26fd536dd6192ec8a57e94141f8b20");
	const pelicula_destacada = await res1.json();
	const res2 = await fetch("https://api.themoviedb.org/3/movie/upcoming?api_key=6f26fd536dd6192ec8a57e94141f8b20");
	const peliculas_proximamente = await res2.json();
	const res3 = await fetch("https://api.themoviedb.org/3/movie/popular?api_key=6f26fd536dd6192ec8a57e94141f8b20");
	const peliculas_populares = await res3.json();

	return {
		props: {
			pelicula_destacada: pelicula_destacada.results,
			peliculas_proximamente: peliculas_proximamente.results,
			peliculas_populares: peliculas_populares.results
		}
	};
}
