import firebase from "firebase";

const config = {
	apiKey: "AIzaSyBug7l1wHFBUPTczhdPL9lPIMIDz9MR254",
	authDomain: "liteflix-8a95f.firebaseapp.com",
	databaseURL: "https://liteflix-8a95f.firebaseio.com",
	projectId: "liteflix-8a95f",
	storageBucket: "liteflix-8a95f.appspot.com",
	messagingSenderId: "101869675802",
	appId: "1:101869675802:web:196ade79981cc21a6478a3"
};

if (!firebase.apps.length) {
	firebase.initializeApp(config);
}
export default firebase;
