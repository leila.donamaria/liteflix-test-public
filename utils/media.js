export const media = {
	smallDevice: "(min-width: 36em)",
	mediumDevice: "(min-width: 48em)",
	largeDevice: "(min-width: 64em)",
	fullHDevice: "(min-width: 90em)"
};
