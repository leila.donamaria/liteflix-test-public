import React, {useContext, useState} from "react";
import Link from "next/link";
import Image from "./Image";
import Button from "./Button";
import {ReactSVG} from "react-svg";
import {pxToRem} from "../utils/pxToRem";
import {ModalContext} from "../utils/context";

export default function MenuMobile() {
	const modalContext = useContext(ModalContext);
	const [menu, setMenu] = useState(false);

	return (
		<header className={menu ? "open" : ""} onClick={() => setMenu(!menu)}>
			<div className="top-bar">
				<button onClick={() => setMenu(!menu)} className="menu-button">
					<div className="bar" />
				</button>
				<h1 className={menu ? "open" : ""}>
					<Link href="/">
						<a title="liteflx">
							<Image folder="general" name="liteflix" ext="png" alt="liteflix" />
						</a>
					</Link>
				</h1>
			</div>
			<nav className={menu ? "open" : ""}>
				<ul>
					<li>
						<button className="user user-active">
							<div className="container-user active">
								<ReactSVG src={`images/icons/user-01.svg`} />
							</div>
							<span>ernesto garmendia</span>
						</button>
					</li>
					<li>
						<Link href="/">
							<a className="user-link">cambiar usuario</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a className="user-link">configuracion</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a className="user-link">ayuda</a>
						</Link>
					</li>
					<li className="novedades">
						<Button text="novedades" icon="bell" styles="novedades" />
					</li>
					<li>
						<Link href="/">
							<a>series</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a>películas</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a>mi lista</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a>niños</a>
						</Link>
					</li>
					<li>
						<Button
							text="agregar película"
							icon="plus"
							styles="add-movie"
							click={() => modalContext.modalDispatch(true)}
						/>
					</li>
					<li>
						<Link href="/">
							<a className="log-out">log out</a>
						</Link>
					</li>
				</ul>
			</nav>
			<style jsx>{`
				.top-bar {
					position: absolute;
					z-index: 3;
					width: 100%;
					height: 40px;

					.menu-button {
						position: absolute;
						left: 14px;
						top: 17px;
						bottom: auto;
						margin: auto;
						background: transparent;
						border: 0;
						padding: 0;
						display: flex;
						justify-content: center;
						align-items: center;
						width: 20px;
						height: 15px;
						transition: 0.5s ease-out;

						&.open {
							.bar {
								transform: translateX(-50px);
								background: transparent;
								&::before {
									transform: rotate(45deg) translate(35px, -35px);
								}
								&::after {
									transform: rotate(-45deg) translate(35px, 35px);
								}
							}
						}

						.bar {
							width: 100%;
							height: 1px;
							background: white;
							transition: 0.5s ease-in-out;
							&::before,
							&::after {
								content: '';
								position: absolute;
								left: 0;
								width: 20px;
								height: 1px;
								background: white;
								transition: 0.5s ease-in-out;
							}
							&::before {
								transform: translateY(-6px);
							}
							&::after {
								transform: translateY(6px);
							}
						}
					}

					h1 {
						width: 95px;
						height: 27px;
						position: absolute;
						top: auto;
						bottom: 0;
						margin: auto;
						line-height: 0;
						transform: translateX(calc((100vw - 95px) / 2));
						transition: transform 0.5s ease-out;
						a {
							text-indent: -99999px;
							line-height: 0;
						}
						&.open {
							transform: translateX(46px);
						}
					}
				}

				header {
					transition: background 0.5s ease-in;
					&.open {
						width: 100%;
						background: var(--black-50);
						min-height: 100vh;
						overflow: hidden;
						position: fixed;
						z-index: 1;
					}
				}

				nav {
					position: absolute;
					z-index: 2;
					top: 0;
					left: 0;
					padding-top: 62px;
					width: 100%;
					max-width: 228px;
					height: 100vh;
					background: var(--black);
					padding-left: 14px;
					transform: translateX(-228px);
					transition: transform 0.5s ease-out;

					&.open {
						transform: translateX(0px);
					}

					ul {
						width: 193px;
					}
					a {
						text-transform: capitalize;
						display: flex;
						flex-direction: row;
						justify-content: flex-start;
						align-items: center;

						&.user-link {
							height: 31px;
							border-bottom: 1px solid var(--dark-gray);
						}

						&:not(.user-link) {
							font-size: ${pxToRem(14)};
							margin-bottom: 12px;
						}

						&.log-out {
							margin-top: 27px;
						}

						&.user-link,
						.log-out {
							font-size: ${pxToRem(12)};
						}
					}

					li:nth-child(9) {
						margin-bottom: 22px;
					}
					.novedades {
						margin-top: 15px;
						button {
							padding: 0;
						}
					}
				}
			`}</style>
		</header>
	);
}
