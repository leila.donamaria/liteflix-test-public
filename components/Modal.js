import React, {useState, useEffect, useContext} from "react";
import dynamic from "next/dynamic";
import {pxToRem} from "../utils/pxToRem";
import {media} from "../utils/media";
import DropZone from "./DropZone";
import Loader from "./Loader";
import Image from "./Image";
import Button from "./Button";
import firebase from "../utils/firebase";
import {ModalContext} from '../utils/context';
import PropTypes from "prop-types";
export default function Modal({ refreshMyFilms }) {
	const DCustomSelect = dynamic(() => import("./CustomSelect.js"), {ssr: false});

	const modalContext = useContext(ModalContext);
	const [file, setFile] = useState(null);
	const [progress, setProgress] = useState("0%");
	const [url, setUrl] = useState("");
	const [uploadError, setUploadError] = useState(false);
	const [film, setFilm] = useState("");
	const [genre, setGenre] = useState(null);
	const [success, setSuccess] = useState(false);

	useEffect(
		() => {
			if (file !== null) {
				handleUploadedFile();
			}
		},
		[file]
	);

	const handleReset = () => {
		setFile(null);
		setProgress("0%");
		setUrl("");
		setUploadError(false);
	};

	const handleCancelUpload = () => {
		if (url !== "") {
			firebase.storage().ref(`images/${file.name}`).delete();
		}
		handleReset();
	};

	const handleSetFile = (file) => {
		setFile(file);
	};

	const handleSubmit = () => {
		let liteflix_array = localStorage.getItem("liteflix");
		let newArray = liteflix_array ? JSON.parse(liteflix_array) : [];
		newArray.push({film, genre, url});
		localStorage.setItem("liteflix", JSON.stringify(newArray));
		setSuccess(true);
		refreshMyFilms();
	};

	const handleUploadedFile = () => {
		let storageRef = firebase.storage().ref(`images/${file.name}`);
		let uploadTask = storageRef.put(file);
		uploadTask.on(
			firebase.storage.TaskEvent.STATE_CHANGED,
			(snapshot) => {
				const progress = `${Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100)}%`;
				setProgress(progress);
			},
			(err) => {
				setProgress(-1);
				setUploadError(true);
			},
			() => {
				firebase.storage().ref(`images/${file.name}`).getDownloadURL().then((url) => {
					setUrl(url);
				});
			}
		);
	};

	const handleSelect = (value) => {
		setGenre(value);
	};

	return (
		<div className="background">
			<div className="container">
				<button className="close" onClick={() => modalContext.modalDispatch(false)}>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.001 512.001">
						<path d="M284.286 256.002L506.143 34.144c7.811-7.811 7.811-20.475 0-28.285-7.811-7.81-20.475-7.811-28.285 0L256 227.717 34.143 5.859c-7.811-7.811-20.475-7.811-28.285 0-7.81 7.811-7.811 20.475 0 28.285l221.857 221.857L5.858 477.859c-7.811 7.811-7.811 20.475 0 28.285a19.938 19.938 0 0014.143 5.857 19.94 19.94 0 0014.143-5.857L256 284.287l221.857 221.857c3.905 3.905 9.024 5.857 14.143 5.857s10.237-1.952 14.143-5.857c7.811-7.811 7.811-20.475 0-28.285L284.286 256.002z" />
					</svg>
				</button>
				{!success ? (
				<>
				{file ? (
					<Loader
						uploadError={uploadError}
						handleCancelUpload={handleCancelUpload}
						handleReset={handleReset}
						progress={progress}
					/>
				) : (
					<DropZone handleSetFile={handleSetFile} />
				)}
				<form>
					<label>
						nombre de la película
						<input
							name="nombre de la pelicula"
							type="text"
							value={film}
							onChange={(e) => setFilm(e.target.value)}
						/>
					</label>
					<label>
						categoria
						<DCustomSelect currentValue={genre} handleSelect={handleSelect} />
					</label>
					<Button
						text="subir pelicula"
						styles="submit"
						disabled={film !== "" && genre !== null && url !== "" ? false : true}
						click={handleSubmit}
					/>
				</form>
				</>) : <div className="container-success">
						<Image folder="general" name="liteflix" ext="png" alt="liteflix"/>
						<p>
							<span className="felicitaciones">Felicitaciones!</span>
							<span>{film}</span> fue correctamente subido a la categoria <span>{genre}</span>
						</p>
						<Button text="cerrar"
						click={() => modalContext.modalDispatch(false)}
						styles="cerrar" />
					</div>}
			</div>
			<style jsx>
				{`
					.background {
						width: 100%;
						height: 100vh;
						position: fixed;
						z-index: 3;
						background: var(--black-80);
						display: flex;
						justify-content: center;
						align-items: center;
					}
					.container {
						z-index: 4;
						width: 100%;
						height: 600px;
						max-height: 100vh;
						position: relative;
						display: flex;
						flex-direction: column;
						justify-content: center;
						align-items: center;
						background: ${!success ? `var(--white)` :`var(--green)`};
						justify-content: space-around;
						padding: 0 16px;
						opacity: 0;
						animation: fadeInToTop 0.4s ease-out forwards;

						label {
							display: inline-block;
							width: 100%;
							max-width: 315px;
							color: var(--gray);
							font-family: var(--monserrat-medium);
							font-weight: bold;
							font-size: ${pxToRem(12)};
							letter-spacing: 5px;
							text-transform: uppercase;
						}

						input[type=text] {
							width: 100%;
							max-width: 315px;
							font-size: ${pxToRem(16)};
							background: transparent;
							color: var(--black);
							padding-bottom: 5px;
							border: 0;
							border-bottom: 2px solid var(--black);
							display: inline-block;
							margin-top: 12px;
							font-family: var(--monserrat-regular);
							font-weight: normal;
							&:focus {
								border-bottom: 2px solid var(--bright-blue);
							}
						}

						&-success {
							width: 100%;
							height: 100%;
							display: flex;
							flex-direction: column;
							justify-content: space-around;
							align-items: flex-start;
							p {
								font-size: ${pxToRem(24)};
								font-family: var(--monserrat-regular);
								max-width: 536px;
								
							.felicitaciones {
								font-size: ${pxToRem(32)};
								font-family: var(--monserrat-bold);
								display: block;
								text-transform: capitalize;
							}
							span {
								font-family: var(--monserrat-bold);
								text-transform: capitalize;
							}
							}
							@media ${media.mediumDevice} {
								justify-content: space-between;
							}
						}
					}

					form {
						display: flex;
						flex-direction: column;
						justify-content: space-between;
						align-items: center;
						min-height: 300px;
						width: 100%;
					}

					.close {
						background: transparent;
						border: 0;
						color: var(--black);
						position: absolute;
						top: 15px;
						right: 15px;
						width: 7px;
						height: 7px;
						cursor: pointer;
						svg {
							width: 7px;
							height: 7px;
							display: block;
						}
					}

					@media ${media.mediumDevice} {
						.container {
							width: 730px;
							height: 354px;
							border-radius: 10px;
							padding: 30px 35px;
						}
						form {
							flex-direction: row;
							flex-wrap: wrap;
							min-height: 152px;
							margin: 30px auto auto;
						}
					}

					
					@keyframes fadeInToTop {
						0% {
							transform: translateY(100px);
							opacity: 0;
						}
						100% {
							transform: translateY(0);
							opacity: 1;
						}
					}
				`}
			</style>
		</div>
	);
}
Modal.propTypes = {
	refreshMyFilms: PropTypes.func
};