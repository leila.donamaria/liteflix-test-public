import React, {useState} from "react";
import Image from "./Image";
import {Waypoint} from "react-waypoint";
import {media} from "../utils/media";
import {pxToRem} from "../utils/pxToRem";
import Button from "./Button";
import PropTypes from "prop-types";

const Arrow = ({style}) => {
	return (
		<div className={style ? `icon ${style}` : `icon`}>
			<svg xmlns="http://www.w3.org/2000/svg" width="13" height="6" viewBox="0 0 13 6">
				<path fill="none" fillRule="evenodd" stroke="#FFF" strokeLinecap="round" d="M1 0l5.546 5.546L12.09 0" />
			</svg>
			<style jsx>{`
				div {
					position: absolute;
					top: auto;
					bottom: 30px;
					left: 0;
					right: 0;
					margin: auto;
					width: 37px;
					height: 18px;
					svg {
						width: 100%;
						height: 100%;
					}
					&.h {
						bottom: -30px;
					}
				}
			`}</style>
		</div>
	);
};
Arrow.propTypes = {
	style: PropTypes.string
};

export const HorizontalGrid = ({films, prop}) => {
	const [enter, setEnter] = useState(false);

	return (
		<div className="container">
			{films.map((film, index) => (
				<div key={index} className="item-container">
					<div className="hover-wrapper">
						<Button icon="plus" styles="lista-film" click={() => console.log("mi lista")} />
						<Button icon="play" styles="play-film h" click={() => console.log("play")} />
						<Button icon="like" styles="like-film h" click={() => console.log("like")} />
						<h4>{film.title ? film.title : film.film}</h4>
						<p>{film.genre}</p>
						<Arrow style="h" />
					</div>
					<Image
						folder={prop === "url" ? "localstorage" : "external"}
						name={film[prop]}
						style="horizontal"
						alt={prop === "url" ? film.film : film.title}
					/>
				</div>
			))}
			<style jsx>
				{`
					.container {
						padding: 0 14px;
						display: grid;
						grid-template-rows: repeat(${films.length}, 210px);
						grid-gap: 10px;
						-webkit-box-shadow: 0px -39px 39px 30px rgba(0, 0, 0, 0.75);
						-moz-box-shadow: 0px -39px 39px 30px rgba(0, 0, 0, 0.75);
						box-shadow: 0px -39px 39px 30px rgba(0, 0, 0, 0.75);
						background: var(--black);
						margin-bottom: ${prop === "url" ? "45px" : "25px"};
						.item-container {
							position: relative;
							width: 100%;
							height: 100%;
							background: var(--black);

							.hover-wrapper {
								width: 100%;
								height: 100%;
								cursor: pointer;
								position: absolute;
								background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.8));
								opacity: 0;
								transition: opacity 0.5s;
								display: flex;
								flex-direction: column;
								justify-content: flex-end;
								align-items: flex-start;
								padding-left: 20px;
								padding-bottom: 30px;
								h4 {
									font-size: ${pxToRem(16)};
									font-family: var(--monserrat-bold);
									text-transform: capitalize;
									max-width: 90%;
									white-space: nowrap;
									text-overflow: ellipsis;
									overflow: hidden;
								}
								p {
									text-transform: capitalize;
								}

								&:hover {
									opacity: 1;
								}
							}
						}

						@media ${media.largeDevice} {
							display: grid;
							grid-template-columns: repeat(4, minmax(10%, 255px));
							grid-template-rows: 155px;
							grid-gap: 31px;
							box-shadow: none;
							background: transparent;
							filter: ${!enter ? `grayscale(100%)` : `grayscale(0)`};
							transition: filter 0.5s;
						}
					}
				`}
			</style>
			<Waypoint onEnter={() => setEnter(true)} onLeave={() => setEnter(false)} />
		</div>
	);
};

HorizontalGrid.propTypes = {
	films: PropTypes.arrayOf(PropTypes.object),
	prop: PropTypes.string
};

export const VerticalGrid = ({films}) => {
	const [enter, setEnter] = useState(false);
	return (
		<div className="container">
			{films.map((film, index) => (
				<div key={index} className="item-container">
					<div className="hover-wrapper">
						<Button icon="play" styles="play-film" click={() => console.log("play")} />
						<Button icon="like" styles="like-film vertical" click={() => console.log("like")} />

						<h4>{film.title}</h4>
						<Arrow />
					</div>
					<Image key={index} folder="external" name={film.poster_path} style="horizontal" alt={film.title} />
				</div>
			))}
			<style jsx>
				{`
					.container {
						padding: 0 14px;
						display: grid;
						grid-template-rows: repeat(2, minmax(10%, 328px));
						grid-template-columns: repeat(2, minmax(calc(50% - 6px), 164px));
						grid-gap: 10px;
						-webkit-box-shadow: 0px -39px 39px 30px rgba(0, 0, 0, 0.75);
						-moz-box-shadow: 0px -39px 39px 30px rgba(0, 0, 0, 0.75);
						box-shadow: 0px -39px 39px 30px rgba(0, 0, 0, 0.75);
						background: var(--black);
						margin-bottom: 70px;
						@media ${media.largeDevice} {
							display: grid;
							grid-template-columns: repeat(4, minmax(10%, 254px));
							grid-template-rows: 400px;
							grid-gap: 31px;
							box-shadow: none;
							background: transparent;
							filter: ${!enter ? `grayscale(100%)` : `grayscale(0)`};
							transition: filter 0.5s;
						}
						.item-container {
							position: relative;
							width: 100%;
							height: 100%;
							background: var(--black);

							.hover-wrapper {
								transform: translateY(0);
								width: 100%;
								height: 100%;
								cursor: pointer;
								position: absolute;
								background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.8));
								opacity: 0;
								transition: opacity 0.5s;
								display: flex;
								flex-direction: column;
								justify-content: flex-end;
								align-items: flex-start;
								padding: 20px;

								h4 {
									max-width: 150px;

									margin-top: 11px;
									font-size: ${pxToRem(18)};
									font-family: var(--monserrat-bold);
									margin-bottom: 60px;
									white-space: nowrap;
									text-overflow: ellipsis;
									overflow: hidden;
								}

								&:hover {
									opacity: 1;
								}
							}
						}
					}
					.icon {
						cursor: pointer;
						background: transparent;
						border: 0;
						width: 40px;
						height: 40px;
						svg {
							width: 100%;
							height: 100%;
							fill: white;
						}
						&::before {
							content: '';
							position: absolute;
							background-color: transparent;
							border: 1px solid var(--white);
							width: 40px;
							height: 40px;
							border-radius: 50%;
							-moz-border-radius: 50%;
							-webkit-border-radius: 50%;
						}
					}
				`}
			</style>
			<Waypoint onEnter={() => setEnter(true)} onLeave={() => setEnter(false)} />
		</div>
	);
};
VerticalGrid.propTypes = {
	films: PropTypes.arrayOf(PropTypes.object)
};
