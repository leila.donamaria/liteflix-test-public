import React from "react";
import Button from "./Button";
import {media} from "../utils/media";
import {pxToRem} from "../utils/pxToRem";

export default function Info({title, overview}) {
	return (
		<div className="flex-container">
			<p className="original">
				original de <strong>liteflix</strong>
			</p>
			<h2>{title}</h2>
			<div className="buttons-container">
				<Button
					text="reproducir"
					icon="play"
					styles="reproducir"
					click={() => console.log("click reproducir")}
				/>
				<Button icon="plus" text="mi lista" styles="lista" click={() => console.log("mi lista")} />
			</div>
			<p className="overview">{overview}</p>
			<style jsx>{`
				.flex-container {
					width: 100%;
					height: 85vh;
					max-height: 645px;
					display: flex;
					flex-direction: column;
					justify-content: flex-end;
					align-items: center;
					padding: 0 14px 51px;
					@media ${media.largeDevice} {
						align-items: flex-start;
						min-height: 600px;
					}
				}

				.buttons-container {
					width: 100%;
					display: flex;
					margin-top: 8px;
					flex-direction: row;
					justify-content: center;
					opacity: 0;
					animation: fadeInToRight 0.7s 0.9s ease-out forwards;
					@media ${media.largeDevice} {
						margin: 19px auto 25px 0;
						max-width: 392px;
						justify-content: flex-start;
					}
				}
				.original {
					font-size: ${pxToRem(18)};
					text-transform: uppercase;
					margin-bottom: 8px;
					opacity: 0;
					animation: fadeInToRight 0.7s 0.5s ease-out forwards;
					strong {
						font-family: var(--monserrat-bold);
					}

					@media ${media.largeDevice} {
						font-size: ${pxToRem(24)};
					}
				}
				h2 {
					font-size: ${pxToRem(72)};
					font-family: var(--robotoslab-regular);
					line-height: 1.2em;
					text-align: center;
					display: -webkit-box;
					-webkit-box-orient: vertical;
					opacity: 0;
					animation: fadeInToRight 0.7s 0.7s ease-out forwards;
					/* to specify the number of lines you want the text to run through... */
					-webkit-line-clamp: 3;
					/* hide the overflowing text, i.e, texts that did not fit in to the box */
					overflow: hidden;
					@media ${media.largeDevice} {
						max-width: 720px;
						-webkit-line-clamp: 2;
						font-size: ${pxToRem(110)};
						text-align: left;
					}
				}
				.overview {
					display: none;
					@media ${media.largeDevice} {
						opacity: 0;
						animation: fadeInToRight 0.7s 1.1s ease-out forwards;
						font-size: ${pxToRem(18)};
						display: block;
						max-width: 537px;
						max-height: 90px;
					}
				}

				@keyframes fadeInToRight {
					0% {
						transform: translateX(-80px);
						opacity: 0;
					}
					100% {
						transform: translateX(0);
						opacity: 1;
					}
				}
			`}</style>
		</div>
	);
}
