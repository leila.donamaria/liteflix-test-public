import React from "react";
import {pxToRem} from "../utils/pxToRem";
import PropTypes from "prop-types";

export default function Loader({progress, handleCancelUpload, handleReset, uploadError}) {
	return (
		<div className="container">
			{!uploadError ? (
				<p>Cargando {progress}</p>
			) : (
				<p>
					<span>Error!</span> No se pudo cargar la película
				</p>
			)}
			<div className="bar">
				<div className={`content-bar ${uploadError ? "fail" : ""}`} />
			</div>
			{!uploadError ? (
				<button onClick={() => handleCancelUpload()} className="accion">
					cancelar
				</button>
			) : (
				<button onClick={() => handleReset()} className="accion">
					reintentar
				</button>
			)}

			<style jsx>
				{`
					p {
						font-size: ${pxToRem(12)};
						text-transform: capitalize;
						color: var(--black);
						margin: 0 auto auto 0;

						span {
							font-family: var(--monserrat-bold);
						}
					}
					.container {
						width: 100%;
						height: 100px;
						border-radius: 10px;
						background: var(--light-gray);
						padding: 15px 30px;
						display: flex;
						flex-direction: column;
						justify-content: center;
						align-items: center;
					}
					.bar {
						width: 100%;
						height: 20px;
						background: var(--gray);
						border-radius: 10px;
						margin: 10px auto;
					}
					.content-bar {
						width: ${progress};
						height: 100%;
						background: var(--green);
						border-radius: 10px;
						transition: width 0.5s ease-in;
						&.fail {
							width: 100%;
							background: red;
						}
					}
					button.accion {
						display: inline-block;
						font-family: var(--monserrat-bold);
						border: 0;
						color: #4a4a4a;
						text-transform: uppercase;
						font-size: ${pxToRem(12)};
					}
				`}
			</style>
		</div>
	);
}
Loader.propTypes = {
	progress: PropTypes.string,
	handleCancelUpload: PropTypes.func,
	handleReset: PropTypes.func,
	uploadError: PropTypes.bool
};
