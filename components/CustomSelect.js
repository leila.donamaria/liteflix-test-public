import React, {useEffect, useState} from "react";
import Select from "react-select";
import PropTypes from "prop-types";
export default function CustomSelect({handleSelect, currentValue}) {
	const [currentVal, setCurrent] = useState(null);

	useEffect(
		() => {
			setCurrent(currentValue);
		},
		[currentValue]
	);

	const options = [
		{value: "accion", label: "Acción"},
		{value: "animacion", label: "Animación"},
		{value: "aventura", label: "Aventura"},
		{value: "ciencia ficcion", label: "Ciencia Ficción"},
		{value: "comedia", label: "Comedia"},
		{value: "documentales", label: "Documentales"}
	];

	const borderStyles = {
		option: (styles, {data}) => {
			if (options[options.length - 1].label == data.label) {
				return {
					...styles,
					border: 0
				};
			}
			return {
				...styles,
				borderBottom: "solid 1px var(--gray)"
			};
		}
	};

	return (
		<span>
			<Select
				options={options}
				classNamePrefix="react-select"
				styles={borderStyles}
				placeholder="Selecciona"
				onChange={(option) => handleSelect(option.value)}
				value={options.filter((obj) => obj.value === currentVal)}
			/>
		</span>
	);
}

CustomSelect.propTypes = {
	handleSelect: PropTypes.func,
	currentValue: PropTypes.string
};
