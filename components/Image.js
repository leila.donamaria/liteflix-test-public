import React from "react";
import PropTypes from "prop-types";

export default function Image({folder, name, ext, style, alt}) {
    
    const renderSrc = () => {
        if(folder === 'external') {
            return `https://image.tmdb.org/t/p/original/${name}`;
        }
        else if(folder === 'localstorage') {
            return name;
        }
        else {
            return `images/${folder}/${name}.${ext}`;
        }
    }
    
    return (
        <>
            <img
                src={renderSrc()}
                className={style}
                alt={alt}
            />
                
            <style jsx>{`

                img {
                    display: inline-block;

                    &.vertical {
                        width: 100%;
					height: 100%;
					position: absolute;
					z-index: 1;
					opacity: 0;
					background-image: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.5));
					color: white;

                    }
                    &.horizontal {
                        width: 100%;
                        height: 100%;
                        object-fit: cover;
                    }
                    &.cover {
                        opacity: 0;
                        animation: fadeIn 0.2s forwards;
                        -webkit-mask-image: linear-gradient(to bottom,rgba(0,0,0,0.7) 87%,var(--black));
                        mask-image: linear-gradient(to bottom,rgba(0,0,0,0.7) 87%, var(--black));
                        
                        position: absolute;
                        width: 100%;
                        max-height: 80vh;
                        min-height: 720px;
                        object-fit: cover;
                        z-index: -1;
                    }

                    &.icon-hover {
                        transform: scale(1.0);
                        transition: transform 0.2s;
                        &:hover {
                            transform: scale(1.05);
                        }
                    }


                    @keyframes fadeIn{
						0% {
							opacity: 0;
						}
						100% {
							opacity: 1;
						}
					}
                }
            `}</style>
        </>
	);
}

Image.propTypes = {
	folder: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    ext: PropTypes.string,
    style: PropTypes.string,
    alt: PropTypes.string
};
Image.defaultProps = {
    ext: 'jpg',
    display: 'inline-block'
  };
  
