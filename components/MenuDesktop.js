import React, {useState, useContext} from "react";
import Link from "next/link";
import Image from "./Image";
import Button from "./Button";
import {media} from "../utils/media";
import {ReactSVG} from "react-svg";
import {pxToRem} from "../utils/pxToRem";
import {ModalContext} from "../utils/context";

export default function MenuDesktop() {
	const modalContext = useContext(ModalContext);
	const [desplegable, setDesplegable] = useState(false);

	return (
		<header>
			<h1>
				<Link href="/">
					<a title="liteflx">
						<Image folder="general" name="liteflix" ext="png" alt="liteflix" />
					</a>
				</Link>
			</h1>
			<nav>
				<ul className="column">
					<li>
						<Link href="/">
							<a>inicio</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a>series</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a>películas</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a>agregados recientemente</a>
						</Link>
					</li>
					<li>
						<Link href="/">
							<a>mi lista</a>
						</Link>
					</li>
					<li>
						<Button
							text="agregar película"
							icon="plus"
							styles="add-movie"
							click={() => modalContext.modalDispatch(true)}
						/>
					</li>
				</ul>

				<ul className="column">
					<li>
						<Link href="/">
							<a>niños</a>
						</Link>
					</li>
					<li>
						<Button icon="bell" styles="novedades hover" click={() => console.log("click novedades")} />
					</li>
					<li>
						<div className="container-user hover" onClick={() => setDesplegable(!desplegable)}>
							<ReactSVG src={`images/icons/user-01.svg`} />
						</div>
					</li>
					<li className="desplegable-container">
						<Button icon="arrow" styles="arrow hover" click={() => setDesplegable(!desplegable)} />
						<ul className="desplegable">
							<li>
								<button className="user user-active">
									<div className="container-user active">
										<ReactSVG src={`images/icons/user-01.svg`} />
									</div>
									<span>ernesto garmendia</span>
								</button>
							</li>
							<li>
								<button className="user">
									<div className="container-user disabled">
										<ReactSVG src={`images/icons/user-01.svg`} />
									</div>
									<span>User 03</span>
								</button>
							</li>
							<li>
								<button className="user">
									<div className="container-user disabled">
										<ReactSVG src={`images/icons/user-01.svg`} />
									</div>
									<span>User 04</span>
								</button>
							</li>
							<li>
								<Link href="/">
									<a>configuración</a>
								</Link>
							</li>
							<li>
								<Link href="/">
									<a className="middle">ayuda</a>
								</Link>
							</li>
							<li>
								<Link href="/">
									<a className="log-out">log out</a>
								</Link>
							</li>
						</ul>
					</li>
				</ul>
			</nav>
			<style jsx>{`
				@media ${media.largeDevice} {
					header {
						width: 100%;
						max-width: 1100px;
						position: absolute;
						z-index: 2;
						height: 40px;
						top: 20px;
						left: 0;
						right: 0;
						bottom: auto;
						margin: auto;
						display: grid;
						grid-gap: 20px;
						grid-template-columns: 95px auto;
						align-items: baseline;
					}

					nav {
						display: flex;
						flex-direction: row;
						justify-content: space-between;
						.column {
							display: grid;
							align-items: center;
							grid-gap: 20px;
						}
						ul:first-child {
							grid-template-columns: repeat(6, auto);
						}
						ul:first-child + ul {
							grid-template-columns: repeat(4, auto);
						}
						a {
							text-transform: capitalize;
						}
					}

					.desplegable {
						opacity: ${desplegable ? "1" : "0"};
						position: absolute;
						top: 50px;
						right: 8px;
						width: 130px;
						height: ${desplegable ? "218px" : "0"};
						border-radius: 5px;
						background-color: var(--white);
						padding: 11px 9px;
						overflow: ${desplegable ? "visible" : "hidden"};
						transition: all 0.2s ease-out;
						a {
							&:hover {
								transform: translateY(0);
							}
						}
						&::before {
							content: '';
							position: absolute;
							z-index: -1;
							top: -7px;
							right: 44px;
							width: 17px;
							height: 17px;
							transform: rotate(-315deg);
							border-radius: 5px;
							background-color: var(--white);
						}
						&-container {
							position: relative;
							&-user {
								cursor: pointer;
							}
						}

						a {
							opacity: ${desplegable ? "1" : "0"};
							transition: all 0.4s ease-in;
							display: block;
							text-transform: capitalize;
							color: var(--black);
							font-size: ${pxToRem(12)};
							padding: 6px 0 7px;
							&.middle {
								border-top: 1px solid #cccccc;
								border-bottom: 1px solid #cccccc;
							}
						}
					}
				}
			`}</style>
		</header>
	);
}
