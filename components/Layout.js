import React from "react";
import globalStyles from "../styles/global.js";
import Head from "next/head";
import dynamic from "next/dynamic";
import withSizes from "react-sizes";

function Layout({children, isMobile}) {
	const DmenuMobile = dynamic(() => import("./MenuMobile"), {ssr: false});
	const DmenuDesktop = dynamic(() => import("./MenuDesktop"), {ssr: false});

	return (
		<div>
			<Head>
				<link rel="icon" href="/favicon.ico" />
				<meta name="og:title" content="Liteflix" />
				<meta name="twitter:card" content="summary_large_image" />
			</Head>
			{isMobile ? <DmenuMobile /> : <DmenuDesktop />}
			<main>{children}</main>
			<style jsx global>
				{globalStyles}
			</style>
		</div>
	);
}
const mapSizesToProps = ({width}) => ({
	isMobile: width < 1024
});
export default withSizes(mapSizesToProps)(Layout);
