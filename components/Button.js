import React from 'react';
import PropTypes from 'prop-types';
import Image from './Image';
import { pxToRem } from '../utils/pxToRem';
import { media } from '../utils/media';

export default function Button({text, icon, styles, disabled, click}) {

    const renderContent = () => {
        if(icon) {
            return <>
                <Image folder="icons" name={icon} ext='png' style="icon-hover" />
                {text ? <span>{text}</span> : null}</>
        }

        return <span>{text}</span>;
    }

    return<>
        <button onClick={() => click()} className={`generic-button ${styles ? styles : ''}`} type="button" disabled={disabled}>
            {renderContent()}
            <style jsx global>{`

                .generic-button {
                    width: 193px;
                    min-height: 38px;
                    box-sizing: border-box;
                    display: flex;
                    border: 0;
                    flex-direction: row;
                    justify-content: flex-start;
                    align-items: center;
                    border-radius: 20px;
                    background: transparent;
                    position: relative;
                    padding-left: ${icon ? "10px" : "0"};
                    span {
                        color: var(--white);
                        text-transform: capitalize;
                        font-size: ${pxToRem(14)};
                        letter-spacing: normal;
                    }
                    img {
                        margin-right: 8px;
                    }

                    &.add-movie {
                        background: var(--red);
                        span {
                            font-size: ${pxToRem(14)};
                        }

                        @media ${media.largeDevice} {
                            width: 40px;
                            position: relative;
                            overflow: hidden;
                            transition: all 0.3s ease-in-out;
                            span {
                                overflow: hidden;
                                display: inline-block;
                                height: 0;
                                opacity: 0;
                                transition: opacity 0.5s ease-in-out, height 0.2s ease;
                            }
                            &:hover {
                                width: 193px;
                                span {
                                    height: 18px;
                                    width: 130px;
                                    opacity: 1;
                                }
                            }
                        }
                    }
                    &.novedades {
                        padding: 0;
                        @media ${media.largeDevice} {
                            width: 14px;
                        }
                    }
                    &.arrow {
                        padding: 0;
                        @media ${media.largeDevice} {
                            width: 20px;
                        } 
                    }
                    &.reproducir {
                        background: var(--black-50);
                        width: 160px;
                        @media ${media.largeDevice} {
							margin-right: 30px;
					}
                    }

                    &.play-film {
                        width: 58px;
                        height: 40px;
                        img {
                            display: block;
                            margin: auto;
                        }
                        transform: scale(1.0);
                        transition: transform 0.2s;
                        &:hover {
                            transform: scale(0.9);
                        }
                        &.h {
                            position: absolute;
                            top: 0;
                            bottom: 0;
                            right: 0;
                            left: 0;
                            margin: auto;
                        }

                        &::after {
                            content: '';
                            position: absolute;
                            background-color: transparent;
                            border:1px solid var(--white);
                            width: 40px; 
                            height: 40px;
                            border-radius:50%;
                            -moz-border-radius:50%;
                            -webkit-border-radius:50%;
                        }
                    }

                    &.like-film {
                        width: 58px;
                        height: 40px;
                        position: absolute;
                        right: 10px;
                        transform: scale(1.0);
                        transition: transform 0.2s;
                        &:hover {
                            transform: scale(0.9);
                        }
                        &.h {
                            top: 10px;
                            width: 32px;
                            height: 32px;
                            padding: 0;
                            img {
                                width: 18px;
                                display: block;
                                margin-left: 8px;
                        

                            }
                            &::after {
                                width: 32px; 
                                height: 32px;
                            }
                        }

                        &.vertical {
                            bottom: 80px;
                        }
                        img {
                            width: 20px;
                            display: block;
                            margin: auto;
                        }
                        &::after {
                            content: '';
                            position: absolute;
                            background-color: transparent;
                            border:1px solid var(--white);
                            width: 40px; 
                            height: 40px;
                            border-radius:50%;
                            -moz-border-radius:50%;
                            -webkit-border-radius:50%;
                        }
                    }

                    &.lista-film {
                        padding: 0;
                        width: 32px;
                        height: 32px;
                        position: absolute;
                        top: 10px;
                        right: 55px;
                        transform: scale(1.0);
                        transition: transform 0.2s;
                        &:hover {
                            transform: scale(0.9);
                        }
                        img {
                            margin: auto;
                        }
                        &::after {
                            content: '';
                            position: absolute;
                            background-color: transparent;
                            border:1px solid var(--white);
                            width: 32px; 
                            height: 32px;
                            border-radius:50%;
                            -moz-border-radius:50%;
                            -webkit-border-radius:50%;
                        }
                    }

                    &.lista {
                        width:45px;
                        height: 30px;
                        position: absolute;
                        right: 15px;
                        img {
                            display: inline-block;
                            margin: auto;
                        }
                        &::after {
                            content: '';
                            position: absolute;
                            background-color: transparent;
                            border:1px solid var(--white);
                            width: 30px; 
                            height: 30px;
                            border-radius:50%;
                            -moz-border-radius:50%;
                            -webkit-border-radius:50%;
                        }
                        span {
                            display: none;
                        }
                        @media ${media.largeDevice} {
                            position: relative;
                            background: var(--black-50);
                            min-height: 40px;
                            width: 160px;
                            &::after {
                                display: none;
                            }
                            img {
                                margin: auto 8px auto 0;
                            }
                            span {
                                display: inline-block;
                            }
                        }
                    }


                    &.submit, &.cerrar {
                        width: 100%;
                        height: 70px;
                        border-radius: 35px;
                        display: flex;
                        flex-direction: row;
                        justify-content: center;

                        background: var(--black);
                        color: var(--white);
                        cursor: pointer;
                    }

                    &.cerrar {
                        max-width: 192px;
                    }

                    &.submit {
                        max-width: 350px;
                        margin-top: 44px;

                        @media ${media.mediumDevice} {
                            margin: 44px auto auto;
                        }
                    }

                    &.submit:disabled {
                        background: var(--gray);
                        cursor: default;
                    }

                }

            `}</style>
        </button>
    </>
}

Button.protoType = {
    icon: PropTypes.string,
    styles: PropTypes.string,
    text: PropTypes.string,
    disabled: PropTypes.bool,
    click: PropTypes.func.isRequired
}

Button.defaultTypes = {
    disabled: false
  };