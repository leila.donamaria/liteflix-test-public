import React from "react";
import PropTypes from "prop-types";
import {pxToRem} from "../utils/pxToRem";

export default function Title({text, style}) {
	return (
		<h3 className={style ? style : ""}>
			{text}
			<style jsx>{`
				h3 {
					font-family: var(--monserrat-bold);
					font-size: ${pxToRem(20)};
					padding-left: 14px;
					text-transform: capitalize;
					margin-bottom: 20px;
					&.populares {
						text-transform: uppercase;
					}
				}
			`}</style>
		</h3>
	);
}

Title.propTypes = {
	text: PropTypes.string.isRequired,
	style: PropTypes.string
};
