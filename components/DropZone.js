import React, {useCallback, useState, useEffect} from "react";
import {useDropzone} from "react-dropzone";
import {media} from "../utils/media";
import PropTypes from "prop-types";
export default function DropZone({handleSetFile}) {
	const [image, setImage] = useState(null);

	useEffect(
		() => {
			handleSetFile(image);
		},
		[image]
	);

	const onDrop = useCallback((acceptedFiles) => {
		setImage(acceptedFiles[0]);
	}, []);

	const {getRootProps, getInputProps} = useDropzone({onDrop});

	return (
		<div className="wrapper" {...getRootProps()}>
			<input {...getInputProps()} />
			<p>
				<Icon />
				<span>Agregar archivo </span> o arrastrarlo y soltarlo aquí
			</p>
			<style jsx>{`
				.wrapper {
					width: 100%;
					height: 100px;
					border-radius: 10px;
					background: var(--white);
					border: 1px dashed var(--gray);
					padding: 15px 30px;
					display: flex;
					flex-direction: column;
					justify-content: center;
					align-items: center;
					color: black;

					p {
						display: inline-block;
						color: var(--gray);
						display: flex;
						justify-content: center;
						alig-items: baseline;
					}

					span {
						margin-right: 5px;
						color: var(--bright-blue);
						font-family: var(--monserrat-bold);
					}
				}

				@media ${media.mediumDevice} {
					.wrapper {
						width: 660px;
						height: 100px;
					}
				}
			`}</style>
		</div>
	);
}

const Icon = () => {
	return (
		<span className="icon">
			<svg height="510pt" viewBox="-25 0 510 510.25747" width="510pt" xmlns="http://www.w3.org/2000/svg">
				<path d="m427.828125 314.484375-37.738281-37.738281-169.816406-169.808594c-31.296876-31.0625-81.816407-30.96875-112.996094.210938-31.179688 31.179687-31.273438 81.699218-.210938 112.996093l169.808594 169.859375c6.945312 6.949219 18.210938 6.949219 25.160156 0 6.945313-6.949218 6.945313-18.210937 0-25.160156l-169.808594-169.859375c-17.367187-17.367187-17.367187-45.519531 0-62.886719 17.367188-17.367187 45.519532-17.367187 62.886719 0l169.859375 169.808594 37.738282 37.734375c31.265624 31.277344 31.253906 81.976563-.019532 113.238281-31.277344 31.261719-81.972656 31.253906-113.238281-.023437l-31.441406-31.453125-176.101563-176.101563-12.582031-12.578125c-43.976563-45.351562-43.417969-117.601562 1.25-162.273437 44.671875-44.667969 116.921875-45.226563 162.273437-1.25l188.679688 188.683593c4.496094 4.492188 11.046875 6.246094 17.183594 4.601563 6.140625-1.644531 10.9375-6.4375 12.582031-12.578125s-.113281-12.6875-4.605469-17.183594l-188.679687-188.679687c-59.089844-58.820313-154.640625-58.710938-213.59375.246093-58.957031 58.953126-59.066407 154.503907-.246094 213.59375l188.679687 188.679688 31.488282 31.453125c45.410156 43.617187 117.375 42.890625 161.890625-1.640625 44.519531-44.527344 45.226562-116.492188 1.597656-161.890625zm0 0" />
			</svg>
			<style jsx>{`
				.icon {
					width: 20px;
					height: 20px;
					display: inline-block;
					margin-right: 7px;
					svg {
						width: 100%;
						height: 100%;
						fill: var(--bright-blue);
					}
				}
			`}</style>
		</span>
	);
};
DropZone.propTypes = {
	handleSetFile: PropTypes.func
};
